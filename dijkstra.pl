#!/usr/bin/env perl

#
# Question 1
#
# In this programming problem you'll code up Dijkstra's shortest-path algorithm.
# Download the text file here. (Right click and save link as).  The file contains
# an adjacency list representation of an undirected weighted graph with 200
# vertices labeled 1 to 200. Each row consists of the node tuples that are
# adjacent to that particular vertex along with the length of that edge. For
# example, the 6th row has 6 as the first entry indicating that this row
# corresponds to the vertex labeled 6. The next entry of this row "141,8200"
# indicates that there is an edge between vertex 6 and vertex 141 that has length
# 8200. The rest of the pairs of this row indicate the other vertices adjacent to
# vertex 6 and the lengths of the corresponding edges.
#
# Your task is to run Dijkstra's shortest-path algorithm on this graph, using 1
# (the first vertex) as the source vertex, and to compute the shortest-path
# distances between 1 and every other vertex of the graph. If there is no path
# between a vertex v and vertex 1, we'll define the shortest-path distance
# between 1 and v to be 1000000.
#
# You should report the shortest-path distances to the following ten vertices, in
# order: 7,37,59,82,99,115,133,165,188,197. You should encode the distances as a
# comma-separated string of integers. So if you find that all ten of these
# vertices except 115 are at distance 1000 away from vertex 1 and 115 is 2000
# distance away, then your answer should be
# 1000,1000,1000,1000,1000,2000,1000,1000,1000,1000. Remember the order of
# reporting DOES MATTER, and the string should be in the same order in which the
# above ten vertices are given. Please type your answer in the space provided.
#
# IMPLEMENTATION NOTES: This graph is small enough that the straightforward O(mn)
# time implementation of Dijkstra's algorithm should work fine. OPTIONAL: For
# those of you seeking an additional challenge, try implementing the heap-based
# version. Note this requires a heap that supports deletions, and you'll probably
# need to maintain some kind of mapping between vertices and their positions in
# the heap.  Answer for Question 1
#
# In accordance with the Honor Code, I certify that my answers here are my own
# work.
#

use strict;
no strict 'refs';
use Data::Dumper;
use Storable 'dclone';

# Root of the graph
my $root;

my ($vertex, $cost);

#
# Sample lines of the adjacency list
#
# 1	80,982	163,8164	170,2620	145,648	200,8021	173,2069	92,647	26,4122	140,546	11,1913	160,6461	27,7905	40,9047	150,2183	61,9146	159,7420	198,1724	114,508	104,6647	30,4612	99,2367	138,7896	169,8700	49,2437	125,2909	117,2597	55,6399
# 2	42,1689	127,9365	5,8026	170,9342	131,7005	172,1438	34,315	30,2455	26,2328	6,8847	11,1873	17,5409	157,8643	159,1397	142,7731	182,7908	93,8177
# 3	57,1239	101,3381	43,7313	41,7212	91,2483	31,3031	167,3877	106,6521	76,7729	122,9640	144,285	44,2165	6,9006	177,7097	119,7711

my $vertex1;
my $vertex2;
my $cost;

# Store lines of the input file adjacency list
my @adjacency_list_input_file_line;

use Heap::Simple;

# Build the adjacency list from the input
while (<>) {

  @adjacency_list_input_file_line = split(/\s+/, $_);

  $vertex1 = shift @adjacency_list_input_file_line;

  foreach my $item (@adjacency_list_input_file_line) {
    ($vertex2, $cost) = split(/,/, $item);
    $root->{$vertex1}->{$vertex2} = $cost;
    $root->{$vertex2}->{$vertex1} = $cost;
  }
}


# You should report the shortest-path distances to the following ten vertices, in
# order: 7,37,59,82,99,115,133,165,188,197. You should encode the distances as a
# comma-separated string of integers. So if you find that all ten of these
# vertices except 115 are at distance 1000 away from vertex 1 and 115 is 2000
# distance away, then your answer should be
# 1000,1000,1000,1000,1000,2000,1000,1000,1000,1000. Remember the order of
# reporting DOES MATTER, and the string should be in the same order in which the
# above ten vertices are given. Please type your answer in the space provided.

# my @find_paths_to_these_verticies = qw(7 37 59 82 99 115 133 165 188 197);

# DEBUGGING WITH TEST DATA
my @find_paths_to_these_verticies = qw(A B C D E F G H);

#
# Source: http://en.wikipedia.org/wiki/Dijkstra's_algorithm
#
# function Dijkstra(Graph, source):
#      for each vertex v in Graph:                                // Initializations
#          dist[v] := infinity ;                                  // Unknown distance function from
#                                                                 // source to v
#          previous[v] := undefined ;                             // Previous node in optimal path
#      end for                                                    // from source
#
#      dist[source] := 0 ;                                        // Distance from source to source
#      Q := the set of all nodes in Graph ;                       // All nodes in the graph are
#                                                                 // unoptimized - thus are in Q
#      while Q is not empty:                                      // The main loop
#          u := vertex in Q with smallest distance in dist[] ;    // Start node in first case
#          remove u from Q ;
#          if dist[u] = infinity:
#              break ;                                            // all remaining vertices are
#          end if                                                 // inaccessible from source
#
#          for each neighbor v of u:                              // where v has not yet been
#                                                                 // removed from Q.
#              alt := dist[u] + dist_between(u, v) ;
#              if alt < dist[v]:                                  // Relax (u,v,a)
#                  dist[v] := alt ;
#                  previous[v] := u ;
#                  decrease-key v in Q;                           // Reorder v in the Queue
#              end if
#          end for
#      end while
#  return dist;

my $INFINITY = 9**9**9;

#
# KEY: Anywhere we update the vertex lookup table, we must also update the heap
#

sub Dijkstra {
  my ($Graph, $source) = @_;

  my ($unknown_dist);
  my ($known_dist) = {};

  foreach my $v (keys(%$Graph)) {
    $unknown_dist->{$v} = $INFINITY;
  }

  # Distance to self is zero
  $unknown_dist->{$source} = 0;

  my $estimated_distances = dclone($Graph);

  my $u = $source;

  # Initialize the fringe
  my $fringe->{$source};

  my $href;

  while (keys(%$unknown_dist) > 0) {

    print "eam File: ", __FILE__, " Line: ", __LINE__, " \$u = $u\n";

    delete $unknown_dist->{$u};
    delete $fringe->{$u};

    my $most_recently_visited_neighbor;

    print "eam File: ", __FILE__, " Line: ", __LINE__, ' {$estimated_distances->{$u}} = ' . Dumper( %{$estimated_distances->{$u}} ). "\n";

    $href = $estimated_distances->{$u};

    foreach my $v (keys(%$href)) {

      my $alt = $estimated_distances->{$v} + $Graph->{$u}->{$v};

      print "eam File: ", __FILE__, " Line: ", __LINE__, " \$u = $u\n";
      print "eam File: ", __FILE__, " Line: ", __LINE__, " \$v = $v\n";
      print "eam File: ", __FILE__, " Line: ", __LINE__, " \$alt = $alt\n";
      print "eam File: ", __FILE__, " Line: ", __LINE__, ' $unknown_dist->{$v} = ' . $unknown_dist->{$v} . "\n";
      print "eam File: ", __FILE__, " Line: ", __LINE__, ' $Graph->{$u}->{$v} = ' . $Graph->{$u}->{$v} . "\n";

      if ($alt < $unknown_dist->{$v}) {

        $unknown_dist->{$v} = $alt;
        $estimated_distances->{$v} = $alt;

      }

      # Can I just arbitrarily go on to expand on the last expanded node's most
      # recently visited neighbor?
      $most_recently_visited_neighbor = $v;
    }

    $u = $most_recently_visited_neighbor;
    print "eam File: ", __FILE__, " Line: ", __LINE__, " \$u = $u\n";
  }

  print "eam File: ", __FILE__, " Line: ", __LINE__, ' $estimated_distances = ', Dumper(%$estimated_distances), "\n";

  return $estimated_distances;
  # return $unknown_dist;
}

print "eam File: ", __FILE__, " Line: ", __LINE__, "\n";
print Dumper $root;

print "eam File: ", __FILE__, " Line: ", __LINE__, "\n";
print Dumper Dijkstra($root, 'A');
exit;

foreach my $find_me (@find_paths_to_these_verticies) {

  print "eam File: ", __FILE__, " Line: ", __LINE__, "\n";
  print Dumper Dijkstra($root, $find_me);

  # DEBUGGING
  last;
}
