package Node;

our $neighbors = {};

sub new {
    my $class = shift;
    return bless {}, $class;
}

sub get_neighbors() {
  return $neighbors;
}

sub add_neighbors {
  my ($neighbor, $cost) = shift;
  $neighbors->{$neighbor} = $cost;
}
